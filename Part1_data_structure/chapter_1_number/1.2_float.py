print(bool(0.2 * 3 == 0.6))

print(bool(1.2 - 0.2 == 1.0))

print(bool(1.2 - 0.1 == 1.1))

print(bool(0.1 * 0.1 == 0.01))

def a(x, y, places=7):
    return round(abs(x-y), places) == 0


# divmod(x, y)
# x는 몫, y는 나머지
print(divmod(45, 6))

# round(x, n)
print(round(100.96, -2))

print(round(100.96, 2))

# 부동소수점을 분수로 표현
print(2.75.as_integer_ratio())