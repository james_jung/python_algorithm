print(sum(0.1 for i in range(10)) == 1.0)


# 부동소수점 불변 타입
# 부동소수점의 반올림, 비교, 뺄셈 등에서 문제를 처리
from decimal import Decimal
print(sum(Decimal("0.1") for i in range(10)) == Decimal("1.0"))