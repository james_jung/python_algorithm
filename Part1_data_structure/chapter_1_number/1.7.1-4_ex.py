def convert_dec_to_any_base_rec(number, base):
    convertString = '012345679ABCDEF'
    if number < base:
        return convertString[number]
    else:
        return convert_dec_to_any_base_rec(number//base, base) \
            + convertString[number % base]


def test_convert_dec_to_any_base_rec():
    number = 9
    base = 2
    assert(convert_dec_to_any_base_rec(number, base) == '1001')
    print('Test passed')


if __name__ == "__main__":
    test_convert_dec_to_any_base_rec()