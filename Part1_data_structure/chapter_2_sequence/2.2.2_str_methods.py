# join()
family = ["James", "Yesom", "Areum"]
print(" ".join(family))
print("-<>-".join(family))
print("".join(family))


# ljust(), rjust()
name = "James"
print(name.ljust(20, "-"))
print(name.rjust(20, "-"))


# format()
print("{0}{1}".format("Hello","Python"))
print("name: {who}, age: {age}".format(who="James", age=36))
print("name: {who}, age: {0}".format(35, who="Yesom"))
print("{} {} {}".format("Python","SQL","Coding"))

import decimal
# s: str / r: repr / a: ascii
print("{0} {0!s} {0!r} {0!a}".format(decimal.Decimal("99.9")))


# unpacking
hero = "batman"
number = 999
print("{number}: {hero}".format(**locals()))


# splitlines()
slayers = "Romeo\nJuliet"
print(slayers.splitlines())


# split()
slayers = "버피*크리스-메리*16"
fields = slayers.split('*')
print(fields)
job = fields[1].split('-')
print(job)


def erase_space_from_string(string):
    s1 = string.split(" ")
    s2 = "".join(s1)
    return s2


start = "Hello*World*!"
print(start.split("*",1))   # 왼쪽에서 첫번째 * 기준으로 split
print(start.rsplit("*",1))  # 오른쪽에서 첫번째 * 기준으로 split


# strip()
slayers = "Romeo & Juliet999"
print(slayers.strip("999"))


import string
import sys

def count_unique_word():
    words = {}
    strip = string.whitespace + string.punctuation + string.digits + "\"'"
    for filename in sys.argv[1:]:
        with open(filename) as file:
            for line in file:
                for word in line.lower().split():
                    word = word.strip(strip)
                    if len(word) > 2:
                        words[word] = words.get(word, 0) + 1

    for word in sorted(words):
        print("{0}: {1}번".format(word, words[word]))


if __name__ == "__main__":
    count_unique_word()


# wapcase()
slayers = "Buffy and Faith"
print(slayers.swapcase())


# index(), find()
# rindex(), rfind() 우측에서부터 검색
print(slayers.find("y"))
print(slayers.find("k"))
# print(slayers.index("k"))    #error: substring not found
print(slayers.index("y"))


# count()
slayers = "Buffy is Buffy is Buffy"
print(slayers.count("Buffy", 0, -1))
print(slayers.count("Buffy"))


# replace()
slayers = "Buffy is Buffy is Buffy"
print(slayers.replace("Buffy","who", 2))


# f-strings    // 파이썬 3.6부터 사용 가능
# .format 대비 간결, 직관적, 속도도 빠름
name = "Fred"
print(f"His name is {name!r}")
print(f"His name is {repr(name)}")

import decimal
width = 10
precision = 4
value = decimal.Decimal("12.34567")
print(f"result: {value:{width}.{precision}}")


from datetime import datetime
today = datetime(year=2021, month=3, day=27)
print(f"{today: %B %d, %Y}")

number = 1024
print(f"{number:#0x}")    # 16진수 표현