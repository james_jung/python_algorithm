import collections

Person = collections.namedtuple('Person', 'name age gender')
p = Person('Astin', 30, 'Man')
print(p)
print(p[0])
print(p.name)