# append()
people = ["buffy", "face"]
people.append("james")
print(people)
people[len(people):] = ["Jander"]
print(people)


# extend()
# iterable
people = ["buffy", "face"]
people.extend("james")
print(people)
people += "wilson"
print(people)


# insert()
people = ["buffy", "face"]
people.insert(1, "prada")
print(people)


# remove()
people = ["buffy", "face"]
people.remove("face")
print(people)


# pop()
people = ["buffy", "face"]
people.pop()
print(people)


# del
a = [-1, 4, 5, 7, 10]
del a[0]
print(a)

del a[2:3]
print(a)


# index()
people = ["buffy", "face"]
print(people.index("buffy"))


# count()
people = ["buffy", "face", "buffy"]
print(people.count("buffy"))


# sort()
people = ["jander", "face", "buffy"]
people.sort()
print(people)
people.sort(reverse=True)    # reverse : 역순
print(people)


# reverse()
people = ["jander", "face", "buffy"]
people.reverse()
print(people)
print(people[::-1])
