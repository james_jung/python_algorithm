first, *rest = [1,2,3,4,5]
print(first)
print(rest)


def example_args(a, b, c):
    return a * b * c


L = [2, 3, 4]
print(example_args(*L))
print(example_args(2, *L[1:]))