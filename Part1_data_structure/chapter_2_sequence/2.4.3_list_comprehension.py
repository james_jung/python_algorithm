# list item
a = [y for y in range(1900, 1940) if y%4 ==0]
print(a)


# list repr
b = [2**i for i in range(13)]
print(b)


# list repr with if sentence
c = [x for x in a if x%2==0]
print(c)


d = [str(round(355/113.0, i)) for i in range(1,6)]
print(d)


words = 'Buffy is awesome and a vampire slayer'.split()
e = [[w.upper(), w.lower(), len(w)] for w in words]
for i in e:
    print(i)



# result = []
# for x in range(10):
#     for y in range(5):
#         if x*y > 10:
#             result.append((x,y))
#
#
# for x in range(5):
#     for y in range(5):
#         if x != y:
#             for z in range(5):
#                 if y != z:
#                     yield (x, y, z)
#
# return ((x, complicated_transform(x))
#         for x in long_generator_function(parameter)
#         if x is not None)
#
# squares = [ x*x for x in range(10)]
#
# eat(jelly_bean for jelly_bean in jelly_beans
#     if jelly_bean.color == 'black')