# 문자열 단어 단위 반전
# 파이썬 문자열 immutable 이므로, mutable인 list를 고려해야 함

def reverser(string1, p1=0, p2=None):
    if len(string1) < 2:
        return string1
    p2 = p2 or len(string1)-1
    while p1 < p2:
        string1[p1], string1[p2] = string1[p2], string1[p1]
        p1 += 1
        p2 -= 1


def reversing_words_sentence_logic(string1):
    reverser(string1)
    print(string1)
    p = 0
    start = 0
    while p < len(string1):
        if string1[p] == u"\u0020":
            reverser(string1, start, p-1)
            print(string1)
            start = p + 1
        p += 1
    reverser(string1, start, p-1)
    print(string1)
    return "".join(string1)


if __name__ == "__main__":
    str1 = "파이썬 알고리즘 정말 재미있다"
    str2 = reversing_words_sentence_logic(list(str1))
    print(str2)


def reverse_words_brute(string):
    word, sentence = [], []
    for character in string:
        if character != " ":
            word.append(character)
        else:
            if word:
                sentence.append("".join(word))
            word = []


    if word != "":
        sentence.append("".join(word))
    sentence.reverse()
    return " ".join(sentence)


if __name__ == "__main__":
    str1 = "파이썬 알고리즘 정말 재미있다"
    str2 = reverse_words_brute(str1)
    print(str2)

def reversing_word_slice(word):
    new_word = []
    words = word.split(" ")
    for word in words[::-1]:
        new_word.append(word)
    return " ".join(new_word)


if __name__ == "__main__":
    str1 = "파이썬 알고리즘 정말 재미있다"
    str2 = reversing_word_slice(str1)
    print(str)


def reversing_words(str1):
    words = str1.split(" ")
    rev_set = " ".join(reversed(words))
    return rev_set


def reversing_words2(str1):
    words = str1.split(" ")
    words.reverse()
    return " ".join(words)


if __name__ == "__main__":
    str1 = "파이썬 알고리즘 정말 재밌다"
    str2 = reversing_words(str1)
    str3 = reversing_words2(str1)
    print(str2)
    print(str3)
