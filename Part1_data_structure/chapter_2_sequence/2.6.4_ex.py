import itertools

# permutation
def perm(s):
    if len(s) < 2:
        return s

    res = []
    for i, c in enumerate(s):
        for cc in perm(s[:i] + s[i+1:]):
            res.append(c + cc)
    return res


def perm2(s):
    res = itertools.permutations(s)
    return ["".join(i) for i in res]


if __name__ == "__main__":
    val = "012"
    print(perm(val))
    print(perm2(val))


# combination
def combinations(s):
    if len(s) < 2:
        return s
    res = []
    for i, c in enumerate(s):
        res.append(c)
        for j in combinations(s[:i] + s[i+1:]):
            res.append(c + j)
    return res


if __name__ == "__main__":
    result = combinations("abc")
    print(result)