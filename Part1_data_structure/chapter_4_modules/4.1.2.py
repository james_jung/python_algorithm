# 좋은 예
# 함수 또는 메서드에 가변 객체를 기본값
def append(number, number_list=None):
    if number_list is None:
        number_list = []
    number_list.append(number)
    return number_list


# 나쁜 예
def append(number, number_list=[]):
    number_list.append(number)
    return number_list