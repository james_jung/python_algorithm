# map(function, list)

def cube(x): return x*x*x

print(list(map(cube, range(1,11))))


# map 메서드의 매개변술를 확인
seq = range(8)    # list 생성
def square(x) : return x*x    # function 생성
print(list(zip(seq, map(square, seq))))