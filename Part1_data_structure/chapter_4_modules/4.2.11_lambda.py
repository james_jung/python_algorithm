# 코드 내에서 함수를 간결하게 사용

# 기존 방법
def area(b, h):
    return 0.5 * b * h
print(area(5, 4))


# lambda 적용
area = lambda b,h: 0.5 * b * h
print(area(5,4))


# lambda 함수 활용 예
import collections
minus_one_dict = collections.defaultdict(lambda: -1)
point_zero_dict = collections.defaultdict(lambda: (0,0))
message_dict = collections.defaultdict(lambda: "No message")