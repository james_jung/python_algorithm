string1, string2, string3 = '', 'monster', 'alien'
non_null = string1 or string2 or string3
print(non_null)

# 파이썬에서 False는
# False
# 숫자 0
# 특수 객체 None
# empty collections ('', [], (), {})


# if not users:
#     print("사용자가 없습니다")
#
#
# if foo == 0:
#     handle_zero()
#
#
# if i % 10 == 0:
#     handle_multiple_of_ten()