# generator : sequence를 반환, 반복문을 사용하는 함수
# iterator : __next__(), __iter__()
# __next__() : 메서드를 호출할 때마다 어떤 값 하나를 추출한 후 해당 yield 표현식의 값을 반환

# 피보나치 제네레이터
def fib_generator():
    a, b = 0, 1
    while True:
        yield b
        a, b = b, a+b


if __name__ == "__main__":
    fib = fib_generator()
    print(next(fib))
    print(next(fib))
    print(next(fib))
    print(next(fib))