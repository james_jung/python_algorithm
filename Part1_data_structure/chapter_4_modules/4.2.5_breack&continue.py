
for i in range(10):
    if i == 4:
        break
    print(i)
else:
    print("for문 종료")

print()

for i in range(10):
    if i % 2 == 0:
        continue
    print(i)
else:
    print("for문 종료")