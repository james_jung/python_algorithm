# 2개 이상의 sequence를 인수로 취하여, 각 항목이 순서대로 1:1 대응하는 새로운 튜플 시퀀스를 생성
a = [1, 2, 3, 4, 5]
b = ['a', 'b', 'c', 'd']
print(zip(a, b))
print(list(zip(a, b)))
