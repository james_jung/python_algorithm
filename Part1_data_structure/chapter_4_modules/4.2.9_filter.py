# sequence 항목들 중 함수 조건이 True인 항목만 추출해서 구성된 시퀀스를 반환
def f(x): return x % 2 != 0 and x % 3 != 0
print(f(33))
print(f(17))
print(list(filter(f, range(2, 25))))