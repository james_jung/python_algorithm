# open()
# open(filename, mode, encoding)
# r: read
# w: write
# a: append
# r+: read, write
# t: text
# b: binary

filename = 'file.txt'
fin = open(filename, encoding='utf8')
fout = open(filename, 'w', encoding='utf8')


# read()
# f.read()


# readline
# 파일에서 한 줄을 읽는다

# readlines
# 파일의 모든 데이터 행을 포함한 리스트를 반환한다
# f.readlines()

# write
# f.write('test\n')


# tell(), seek()
# tell(): 파일의 현재 위취를 나타내는 정수를 반환
# seek(offset, from-what): 파일 내 탐색 위치를 변경할 때 사용

# close()
# 파일을 닫고, 열린 파일이 차지하는 시스템 자원을 해제
# 파일을 성공적으로 닫으면 True를 반환

# input()
# 사용자 입력을 받음

# peek()
# 파일 포인터 위치를 이동하지 않고, n바이트를 반환

# fileno()
# 파일 서술자를 반환