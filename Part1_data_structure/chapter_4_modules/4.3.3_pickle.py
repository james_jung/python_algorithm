import pickle
# pickle: 파이썬 객체를 가져와서 문자열 표현으로 변환
# 피클링, 직렬화, Serialization
# 언피클링, 역직렬화, Deserialization


x = {}
x["name"] = "James"
x["age"] = 23
with open("name.pkl", "wb") as f:
    pickle.dump(x, f)


with open("name.pkl", "rb") as f:
    name = pickle.load(f)

print(name)