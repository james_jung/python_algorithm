# struct
# 파이썬 객체 -> 이진 표현으로 변환
# 이진 표현 -> 파이썬 객체로 변환


import struct

abc = struct.pack('>hhl', 1, 2, 3)
print(abc)

print(struct.unpack('>hhl', abc))

print(struct.calcsize('>hhl'))