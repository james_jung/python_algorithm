# syntax error : 구문 분석 오류
# exception : 실행 중 발견되는 오류
# try - except - finally


# while True:
#     try:
#         x = int(input("숫자를 입력하세요: "))
#         break
#     except ValueError:
#         print("숫자가 아닙니다. 다시 입력해주세요")


import string
import sys


filename = 'myfile.txt'
try:
    1/0
    f = open(filename, 'r')
    s = f.readline()
    i = int(string.strip(s))
except IOError as err:
    print(err)
    print("{0}를 열 수 없습니다".format(filename))
except ValueError:
    print("데이터를 숫자로 변환할 수 없습니다")
except:
    print("예외 발생: {0}".format(sys.exc_info()[0]))
    raise
else:
    f.close()