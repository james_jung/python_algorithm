from Part2_algorithm.chapter_7_abstract_data.queue.queue import Queue

class Deque(Queue):
    def enqueue_back(self, item):
        self.items.append(item)

    def dequeue_front(self):
        value = self.items.pop(0)
        if value is not None:
            return value
        else:
            print("Deque is empty")


if __name__ == "__main__":
    deque = Deque()
    print(deque.isEmpty())
    for i in range(10):
        deque.enqueue(i)
    print(deque.size())
    print(deque.peek())
    print(deque.dequeue())
    print(deque.peek())
    print(deque.isEmpty())
    print(deque.dequeue_front())
    print(deque.peek())
    deque.enqueue_back(50)
    print(deque.peek())
    print(deque)

