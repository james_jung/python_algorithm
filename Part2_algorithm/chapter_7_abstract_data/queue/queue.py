class Queue(object):
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return not bool(self.items)

    def enqueue(self, item):
        self.items.insert(0, item)

    def dequeue(self):
        value = self.items.pop()
        if value is not None:
            return value
        else:
            print("Queue is empty")

    def size(self):
        return len(self.items)

    def peek(self):
        if self.items:
            return self.items[-1]

    def __repr__(self):
        return repr(self.items)


if __name__ == "__main__":
    queue = Queue()
    print(queue.isEmpty())
    for i in range(10):
        queue.enqueue(i)
    print(queue.size())
    print(queue.peek())
    print(queue.dequeue())
    print(queue.peek())
    print(queue.isEmpty())
    print(queue)