
def balance_par_str_with_stacks(str1):
    s = []
    balanced = True
    index = 0

    while index < len(str1) and balanced:
        symbol = str1[index]

        if symbol == "(":
            s.append(symbol)

        else:
            if len(s) == 0:
                balanced = False
            else:
                s.pop()

        index += 1

    if balanced and len(s) == 0:
        return True

    else:
        return False


if __name__ == "__main__":
    print(balance_par_str_with_stacks('((()))'))
    print(balance_par_str_with_stacks('(()'))