
def dec2bin_with_stack(decnum:int) -> int:
    s = []
    str_aux = ''

    while decnum > 0:
        dig = decnum % 2
        decnum //= 2
        s.append(dig)

    while len(s) > 0:
        str_aux += str(s.pop())

    return str_aux


if __name__ == "__main__":
    decnum = 9
    print(dec2bin_with_stack(decnum))