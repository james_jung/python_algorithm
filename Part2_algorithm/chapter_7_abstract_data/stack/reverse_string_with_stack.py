# from .stack import Stack


def reverse_string_with_stack(str1):
    s = []
    revStr = ''

    for c in str1:
        # s.push(c)
        s.append(c)

    while len(s)>0:
        revStr += s.pop()

    return revStr


if __name__ == "__main__":
    str1 = "buffy is the angel"
    print(str1)
    print(reverse_string_with_stack(str1))